var JoinUsSection = (function() {
    function init(typeText, buttonText) {

        var footer = document.getElementsByClassName('app-footer')[0];
        var joinSection = document.createElement('div');
        var mainText = document.createElement('h1');
        var secondaryText = document.createElement('p');
        var emailForm = document.createElement("FORM");
        var emailInput = document.createElement("INPUT");
        var emailButton = document.createElement("BUTTON");

        joinSection.setAttribute('id', 'join');
        mainText.setAttribute('id', 'mainText');
        secondaryText.setAttribute('id', 'secondaryText');

        emailForm.setAttribute("id", "myForm");
        emailInput.setAttribute("id", "emailInput");
        emailInput.setAttribute("type", "text");
        emailInput.setAttribute("placeholder", "Email");
        emailButton.setAttribute('id', 'emailButton');

        emailButton.innerHTML = buttonText;
        mainText.innerHTML = typeText;
        secondaryText.innerHTML = `Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.`;

        emailForm.addEventListener("submit", function(e){
            console.log(emailInput.value);
            e.preventDefault();
            emailInput.value = "";
        }, false);

        joinSection.appendChild(mainText);
        joinSection.appendChild(secondaryText);
        joinSection.appendChild(emailForm);
        document.body.appendChild(joinSection);
        emailForm.appendChild(emailInput);
        emailForm.appendChild(emailButton);
        footer.parentNode.insertBefore(joinSection, footer);
    }

    return {
        init: init
    };
})();

export default JoinUsSection;