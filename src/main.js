import JoinUsSection from './joinussection.js';

document.addEventListener("DOMContentLoaded", function() {
    class SectionCreator {
        create(type) {
            switch(type){
                case 'standard':
                    return JoinUsSection.init("Join Our Program", "SUBSCRIBE");
                case 'advanced':
                    return JoinUsSection.init("Join Our Advanced Program", "SUBSCRIBE TO ADVANCED PROGRAM");
            }
        }
        remove(){
                let el = document.getElementById('join');
                el.remove();
        }
    }
    const sectionFactory = new SectionCreator();
    sectionFactory.create('advanced');
    sectionFactory.create('standard');
    
    sectionFactory.remove();
    // sectionFactory.remove();
});

